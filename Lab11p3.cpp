// File: Lab11p3.cpp
// Created by Robert "Christian" Di Lorenzo on 02/27/2014
// This program calculates an athlete's score.
// NOTE: REQUIRES COMPILATION WITH C++ 11!

#include <cstdlib>
#include <iostream>
#include <vector>
using namespace std;

float minValue(vector<float> numArray)
{
  float min = numArray.front();
  for (float i : numArray) if (i < min) min = i;
  return min;
}

float maxValue(vector<float> numArray)
{
  float max = numArray.front();
  for (float i : numArray) if (i > max) max = i;
  return max;
}

float average(vector<float> numArray)
{
  float total = 0.0;
  for (float i : numArray) total += i;
  return total / numArray.size();
}

int main()
{
  float score = 0;
  vector<float> scores;

  cout << "Please enter five scores." << endl;

  cout << "First score: ";
  cin >> score; scores.push_back(score);

  cout << "Second score: ";
  cin >> score; scores.push_back(score);

  cout << "Third score: ";
  cin >> score; scores.push_back(score);

  cout << "Fourth score: ";
  cin >> score; scores.push_back(score);

  cout << "Five score: ";
  cin >> score; scores.push_back(score);

  cout << "\nLowest score: " << minValue(scores) << endl;
  cout << "Highest score: " << maxValue(scores) << endl;

  sort(scores.begin(), scores.end());
  vector<float> sub(&scores[1], &scores[4]);
  cout << "Average score with highest and lowest score excluded: " << average(sub) << endl << endl;
  
  system("pause");
  return 0;
}
