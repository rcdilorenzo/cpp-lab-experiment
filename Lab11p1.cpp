// File: Lab11p1.cpp
// Created by Robert "Christian" Di Lorenzo on 02/27/2014
// This program calculates the winner(s) in an election.
// NOTE: REQUIRES COMPILATION WITH C++ 11!

#include <cstdlib>
#include <iostream>
#include <vector>
using namespace std;

int maxValue(vector<int> numArray)
{
  int max = numArray.front();
  for (int i : numArray) if (i > max) max = i;
  return max;
}

vector<int> maxIndexes(vector<int> numArray)
{
  int winningVotes = maxValue(numArray);
  vector<int> indexes;
  int count = 0;
  for (int nomineeVotes : numArray)
  {
    if (nomineeVotes == winningVotes)
      indexes.push_back(count);
    count++;
  }
  return indexes;
}

int main()
{
  vector<string> names {"Andy", "Brianna", "Chuck"};
  vector<int> votes;

  for (string name : names)
  {
    int voteCount;
    cout << "How many votes did " << name << " receive? ";
    cin >> voteCount;
    votes.push_back(voteCount);
  }

  auto winnerIndexes = maxIndexes(votes);
  if (winnerIndexes.size() > 1)
  {
    int count = 0;
    for (int index : winnerIndexes)
    {
      if (count != 0 && winnerIndexes.size() > 2) cout << ",";
      if (count == winnerIndexes.size()-1) cout << " and";
      if (count != 0) cout << " ";
      cout << names.at(index);
      count++;
    }
    cout << " tied for first place." << endl;
  }
  else
  {
    cout << names.at(winnerIndexes.front()) << " has won the election!" << endl;
  }
  
  system("pause");
  return 0;
}
