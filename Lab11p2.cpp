// File: Lab11p2.cpp
// Created by Robert "Christian" Di Lorenzo on 02/27/2014
// This program finds the smallest numbers in a group.
// NOTE: REQUIRES COMPILATION WITH C++ 11!

#include <cstdlib>
#include <iostream>
#include <vector>
using namespace std;

int minValue(vector<int> numArray)
{
  int min = numArray.front();
  for (int i : numArray) if (i < min) min = i;
  return min;
}

int main()
{
  int number = 0;
  vector<int> numbers;

  cout << "Please enter five numbers." << endl;

  cout << "First number: ";
  cin >> number; numbers.push_back(number);

  cout << "Second number: ";
  cin >> number; numbers.push_back(number);

  cout << "Third number: ";
  cin >> number; numbers.push_back(number);

  cout << "Fourth number: ";
  cin >> number; numbers.push_back(number);

  cout << "Five number: ";
  cin >> number; numbers.push_back(number);

  cout << "Smallest number: " << minValue(numbers) << endl;
  
  system("pause");
  return 0;
}
